import sys
import glob
import serial
import requests
import time


def portscan():
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
        print('win')
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        ports = glob.glob('/dev/tty[A-Za-z]*')
        print('linux')
    else:
        raise EnvironmentError('Unsupported platform')
        print('Unsupported platform')

    result = []
    for port in ports:
        try:
            ser = serial.Serial(port)
            print(port)
            ser.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


while True:
    for port in portscan():
        print("Try to connect port" + port, end=" ")
        ser = serial.Serial(port,9600)
        try:
            ser.in_waiting > 0
            print("OK! Connected.")
            while True: 
                val = ser.readline().strip().decode('UTF-8')
                try:
                    print('http://localhost/index.php/insert?voltage=' + val)
                    req = requests.get('http://localhost/index.php/insert?voltage=' + val)
                    print(req)
                except:  # This is the correct syntax
                    print("Error")
        except:
            print('ERROR! Port not avaible.')
    time.sleep(5)