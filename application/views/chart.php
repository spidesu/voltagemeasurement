<html>
<head>
<meta charser="utf-8">
<link rel="stylesheet" href="<?php base_url()?>/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php base_url()?>/assets/css/Chart.min.css">
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js')?>"></script>
<script src="<?php base_url()?>/assets/js/popper.min.js"></script>
<script src="<?php base_url()?>/assets/js/bootstrap.min.js"></script>
<script src="<?php base_url()?>/assets/js/moment.js"></script>
<script src="<?php base_url()?>/assets/js/Chart.min.js"></script>
</head>
<body>
<div style="width: 100%; display:flex;">
<div>
<canvas id="myChart" width="800" height="400"></canvas>
    <div class="control">

    <label for="tempDate">Дата:</label><input name="tempDate" id="tempDate" type="date">
    <label for="tempTimeStart">Время от:</label><input name="tempTimeStart" id="tempTimeStart" type="time">
    <label for="tempTimeEnd">Время до:</label><input name="tempTimeEnd" id="tempTimeEnd" type="time">
     <button onclick="update()">Обновить</button>
    </div>
</div>
    <div id="messages">
    Проблемы с напряжением:<br>
    <div id="mainMessage">
    </div>
   
  
    </div>
</div>
<script>

var ctx = document.getElementById('myChart');


let chart = new Chart(ctx, {
    type: 'line',
    data: {
        datasets: [{
            label: 'Напряжение',
            data: []
        }],
        labels: []
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    suggestedMin: 0,
                    suggestedMax: 5
                }
            }]
        }
    }
});

function addData(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}

function removeData(chart) {
    chart.data.labels=[];
    chart.data.datasets.forEach((dataset) => {
        dataset.data=[];
    });
    chart.update();
}

$(document).ready(function() {
$.get("index.php/getValue?tempTimeStart=&tempTimeEnd=", function (data) {
    json_res = data;
    json_res = JSON.parse(json_res);
    json_res.forEach(function (item, i, json_res) {
        addData(chart,item.tm, item.avgValues); 
    })
});
});

function update() {
    removeData(chart);
    start= document.getElementById('tempDate').value + ' ' + document.getElementById('tempTimeStart').value;
    end= document.getElementById('tempDate').value + ' ' + document.getElementById('tempTimeEnd').value;
    $.get("index.php/getValue?tempTimeStart=" + start + "&tempTimeEnd=" + end, function (data) {
    json_res = data;
    json_res = JSON.parse(json_res);
    json_res.forEach(function (item, i, json_res) {
        addData(chart,item.tm, item.avgValues); 
    })
});
}

setInterval(
    function() {
        var messages = document.getElementById("mainMessage");
        messages.innerHTML = "";
    $.get("index.php/getProblems", function (data) {
    json_res = data;
    json_res = JSON.parse(json_res);
  
    json_res.forEach(function (item, i, json_res) {
         var newMessage = document.createElement("p");
         newMessage.innerHTML = "Напряжение: " + item.voltage + "В Дата: " +item.date;
         messages.appendChild(newMessage);
    })
});

    },10000);
</script>
</body>
