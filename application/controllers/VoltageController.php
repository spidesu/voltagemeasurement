<?php

class VoltageController extends CI_Controller {
   
    public function _construct() {
        parent::_construct;
        $this->load->helper('url');
    }

    public function insert_value() {
        $this->load->helper('file');
        $this->load->model('voltage');
        $this->load->helper('url');
        $voltage = $_GET['voltage'];
        if ($voltage<=2.5) {
            $problem['voltage']=$voltage;
            $problem['date']=date("Y-m-d H:i:s");
            $base_url = base_url();
            $file = file_get_contents("./assets/json/problems.json");
            $problems = json_decode($file, TRUE);
            unset($file);
            var_dump($problem);
            $problems[] = $problem;
            $file = json_encode($problems);
            var_dump($file);
            file_put_contents("./assets/json/problems.json", $file);
        }
        $this->voltage->insert_entry($voltage);
    }

    public function index() {
        $this->load->helper('url');
        $this->load->view('chart');
    }
    
    public function getValue() {
        $timestampStart = $_GET['tempTimeStart'];
        $timestampEnd = $_GET['tempTimeEnd'];
    
        $this->load->model('voltage');
        $result = $this->voltage->get_avg_dataset($timestampStart, $timestampEnd);
        echo json_encode($result);

}
    public function getProblems() {
        $file = file_get_contents("./assets/json/problems.json");
        echo $file;
    }
}