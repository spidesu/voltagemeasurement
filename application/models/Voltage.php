<?php
class Voltage extends CI_model {

    public $voltage;
    public $date;
    public function insert_entry()
    {
        $this->load->helper('date');
        $this->voltage = $_GET['voltage'];
        $this->db->insert('voltage', $this);
    }

    public function get_avg_dataset($timestampStart, $timestampEnd) {
        $hours='%H:%i';
        if($timestampStart == null || $timestampEnd == null) {
            $timestampEnd = date("Y-m-d H:i:s",time());
            $timestampStart = date("Y-m-d H:i:s",time()-1600);
           
        }
if(strtotime($timestampEnd) - strtotime($timestampStart) >3600) {
	$hours='%H:00';
}
        $query=$this->db->query(
    "SELECT 
        date_format(date,'$hours') as tm,
        AVG(voltage) as avgValues
    FROM   
        voltage
    WHERE  
        date BETWEEN '$timestampStart' AND '$timestampEnd' 
    GROUP BY 
        tm
    ORDER BY 
        tm ASC ");

        return $query->result();
    }
}
?>
